-- | Evaluation.hs

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Evaluation where

import           Fetch
import qualified Data.Text                      as T
import qualified Data.Map.Strict                as M
import qualified Data.IntMap.Strict             as IM
import           Data.Maybe         (catMaybes)
import           Query
import           Data.List          (sortBy)
import           Data.Ord           (comparing)

type SearchResult = [(FilePath, Rational, PageText)]
type TermFrequencyIndex = M.Map FilePath Rational
type FullOccurences = M.Map FilePath Int

--fst' (x,_,_) = x
--snd' (_,x,_) = x
--thd  (_,_,x) = x


-- Query Evaluation
evaluateQuery :: Query -> AllDaemonData -> SearchResult
evaluateQuery query = sortScore . filterEmptyPageText
                                . fst
                                . evaluateQueryIn query

sortScore :: SearchResult -> SearchResult
sortScore = sortBy (flip $ comparing (\(_,r,_)->r))

filterEmptyPageText :: SearchResult -> SearchResult
filterEmptyPageText = filter (\(_,_,pt) -> not $ IM.null pt)

evaluateQueryIn :: Query -> AllDaemonData -> ( SearchResult
                                             , DocumentMap
                                             )
                                             --, AllTermFrequency )
evaluateQueryIn query@Query{within=Nothing} add = evaluateQueryRaw query add
evaluateQueryIn query@Query{within=Just  i} add = eQueryRaw
  where
    eQueryRaw = evaluateQueryRaw query
                                 $ add { documentMap = snd $ eQueryWithin }
    eQueryWithin = evaluateQueryIn i add

evaluateQueryRaw :: Query -> AllDaemonData -> ( SearchResult
                                              , DocumentMap
                                              )
                                              --, AllTermFrequency )
evaluateQueryRaw query add =
  ( combineEvaluatedTerms evalTerms
  , getDocumentMap
  --, M.intersection (allTermFrequency add) getDocumentMap
  )
  where
    getDocumentMap = combineDocumentMaps $ map snd evalTerms
    evalTerms = evaluateQueryTerms (fuzzy query)
                                   (exact query)
                                   add
    --changeDataToEval = add { invertedIndex       = evalExcludeExact
    --                       , suffixInvertedIndex = evalExcludeFuzzy
    --                       }
    --evalFPath = evaluateFPath (fPath query) add
    --evalExcludeFuzzy = evaluateExclude (exclude query)
    --                                   $ suffixInvertedIndex add
    --evalExcludeExact = evaluateExclude (exclude query)
    --                                 $ invertedIndex add

evaluateQueryTerms :: [Term] -> [Term] -> AllDaemonData
                   -> [(TermFrequencyIndex, DocumentMap)]
evaluateQueryTerms fuzzy exact add = evaluateFuzzy ++ evaluateExact
  where
    evaluateFuzzy = evaluateTermWithAdd True fuzzy suffixInvertedIndex
    evaluateExact = evaluateTermWithAdd False exact invertedIndex
    evaluateTermWithAdd isFuzzy term invInd =
      evaluateTerms isFuzzy
                    term
                    (invInd add)
                    (allTermFrequency add)
                    $ documentMap add

--evaluateFPath :: [FilePath] -> AllDaemonData -> AllDaemonData
--evaluateFPath fPath add =
--  add { documentMap      = filterFPath (documentMap add)
--      , allTermFrequency = filterFPath (allTermFrequency add)
--      }
--  where
--    filterFPath = M.filterWithKey (\k _ -> k `elem` fPath)

--evaluateExclude :: [Term] -> InvertedIndex -> InvertedIndex
--evaluateExclude term = M.filterWithKey (\t _ -> not $ t `elem` term)

--evaluateExclude :: [Term]


-- Evaluation Combination
--testEvaluateSearch sterms = do
--  invIndex <- fetchSuffixInvertedIndex
--  aTF <- fetchAllTermFrequency
--  dMap <- fetchDocumentMap
--  return $ evaluateSearch (map T.pack sterms) <$> invIndex <*> aTF <*> dMap

evaluateSearch ::
  [Term]
  -> InvertedIndex
  -> AllTermFrequency
  -> DocumentMap
  -> SearchResult
evaluateSearch terms invIndex aTF dMap =
  sortScore $ combineEvaluatedTerms $ evaluateTerms True
                                                    terms
                                                    invIndex
                                                    aTF
                                                    dMap

combineEvaluatedTerms :: [(TermFrequencyIndex, DocumentMap)]
  -> SearchResult
combineEvaluatedTerms eval = combineTFIandDMap (termFrequencyIndices eval)
                                               $ documentMaps eval
  where
    termFrequencyIndices = combineTermFrequencyIndices . map fst
    documentMaps = combineDocumentMaps . map snd

combineTFIandDMap :: TermFrequencyIndex -> DocumentMap -> SearchResult
combineTFIandDMap tfi dMap = catMaybes
                             $ map (\(fp,r) -> (fp, r,) <$> dMap M.!? fp)
                             $ M.toList tfi

combineTermFrequencyIndices :: [TermFrequencyIndex] -> TermFrequencyIndex
combineTermFrequencyIndices = M.unionsWith (+)

combineDocumentMaps :: [DocumentMap] -> DocumentMap
combineDocumentMaps = M.unionsWith (\pt pt2 -> IM.union pt pt2)


-- Evaluation Construction
--testEvaluateTerms sterms = do
--  invIndex <- fetchLcInvertedIndex
--  aTF <- fetchAllTermFrequency
--  dMap <- fetchDocumentMap
--  return $ evaluateTerms (map T.pack sterms) <$> invIndex <*> aTF <*> dMap

evaluateTerms ::
  Bool
  -> [Term]
  -> InvertedIndex
  -> AllTermFrequency
  -> DocumentMap
  -> [(TermFrequencyIndex, DocumentMap)]
evaluateTerms isFuzzy terms invIndex aTF dMap =
  catMaybes $ map (\t -> evaluateTerm isFuzzy t invIndex aTF dMap) terms

--testEvaluateTerm sterm = do
--  invIndex <- fetchLcInvertedIndex
--  aTF <- fetchAllTermFrequency
--  dMap <- fetchDocumentMap
--  return $ evaluateTerm (T.pack sterm) <$> invIndex <*> aTF <*> dMap

evaluateTerm ::
  Bool
  -> Term
  -> InvertedIndex
  -> AllTermFrequency
  -> DocumentMap
  -> Maybe (TermFrequencyIndex, DocumentMap)
evaluateTerm isFuzzy term invIndex aTF dMap = (,) <$> tFreqIndex match aTF
                                                  <*> ocFilter match dMap
  where
    match = matchTerm isFuzzy term invIndex
    tFreqIndex Nothing   _ = Nothing
    tFreqIndex (Just oc) a = Just $ termFrequencyIndex
                                    (mergePageOnOccurences oc) a
    ocFilter Nothing   _ = Nothing
    ocFilter (Just oc) d = Just $ filterOnOccurence oc d


-- Ranking
termFrequencyIndex ::
  FullOccurences -> AllTermFrequency -> TermFrequencyIndex
termFrequencyIndex fo aTF =
  --M.mapWithKey (\f no -> tfidf f no aTF (M.size aTF) fo) fo
  M.mapWithKey (\f no -> bm25 f no aTF fo) fo

bm25 :: FilePath -> Int -> AllTermFrequency -> FullOccurences -> Rational
bm25 fp frq aTF fo = bm25Raw (toRational 1.6) (toRational 0.75) fp frq aTF fo

bm25Raw ::
  Rational
  -> Rational
  -> FilePath
  -> Int
  -> AllTermFrequency
  -> FullOccurences
  -> Rational
bm25Raw k b fp frq aTF fo = bm25idf aTF fo * (upper / lower)
  where
    upper = (toRational frq) * (k + 1)
    lower = (toRational frq) + k * (1 - b + b * ((toRational $ aTF M.! fp)
                                                 / avgdl aTF))

avgdl :: AllTermFrequency -> Rational
avgdl aTF = (toRational $ sum $ M.elems aTF) / (toRational $ M.size aTF)

bm25idf :: AllTermFrequency -> FullOccurences -> Rational
bm25idf aTF fo = toRational
  $ log $ 1 + ((fromIntegral $ M.size aTF) - (fromIntegral $ M.size fo) + 0.5
               / (fromIntegral $ M.size fo) + 0.5)

--tfidf ::
--  FilePath -> Int -> AllTermFrequency -> Int -> FullOccurences -> Rational
--tfidf fp frq atf n oc = (tf fp frq atf) * (idf n oc)
--
--tf :: FilePath -> Int -> AllTermFrequency -> Rational
--tf fp frq atf = (toRational frq) / (toRational $ atf M.! fp)
--
--idf :: Int -> FullOccurences -> Rational
--idf n oc = toRational $ log $ (fromIntegral n) / (fromIntegral $ M.size oc)


-- Matching/Filtering
filterOnOccurence :: Occurences -> DocumentMap -> DocumentMap
filterOnOccurence occurs = M.mapWithKey (\fp pt -> match fp pt occurs)
  where
    match f pt o = IM.filterWithKey (\p _ -> case matchFile f o of
                                               Nothing -> False
                                               Just m  -> IM.member p m) pt
    matchFile fn o = o M.!? fn

--testMatchTerm term index = fmap (matchTerm $ T.pack term) <$> index
--testMergeMatchTerm term index = fmap (mergeMatchTerm $ T.pack term) <$> index

matchTerm :: Bool -> Term -> InvertedIndex -> Maybe Occurences
matchTerm False term index = index M.!? term
matchTerm True  term index = Just $ M.unionsWith (IM.unionWith (+))
                                  $ filterPrefixFromTerm term index

filterPrefixFromTerm :: Term -> InvertedIndex -> InvertedIndex
filterPrefixFromTerm term = M.filterWithKey (\k _ -> T.isPrefixOf term k)

--mergeMatchTerm :: Term -> InvertedIndex -> FullOccurences
--mergeMatchTerm term index = mergePageOnOccurences $ matchTerm term index

mergePageOnOccurences :: Occurences -> FullOccurences
mergePageOnOccurences = M.map totalPageOccurences
  where
    totalPageOccurences = sum . IM.elems
