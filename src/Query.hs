-- | Query.hs

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveAnyClass #-}

module Query where

--import Control.Applicative
--import Control.Monad
import           Text.Megaparsec
import           Text.Megaparsec.Char
import qualified Data.Text                        as T
import           Data.Void
import           Data.Maybe           (catMaybes)

data Query = Query
  { fuzzy   :: [T.Text]
  , exact   :: [T.Text]
  --, exclude :: [T.Text]
  , within  :: Maybe Query
  } deriving (Eq, Show)

data QueryValue
  = QueryFuzzy [T.Text]
  | QueryExact [T.Text]
  -- | QueryExclude [T.Text]
  | QueryWithin T.Text
  deriving (Eq, Show)

type Parser = Parsec Void T.Text

parseQuery :: Parser Query
parseQuery = do
  raw <- parseQueryValueList
  --fPath    <- pure $ concat $ [x | QueryFPath   x <- fst raw]
  fuzzyCmd <- pure $ concat $ [x | QueryFuzzy x <- fst raw]
  exact    <- pure $ concat $ [x | QueryExact x <- fst raw]
  --exclude  <- pure $ concat $ [x | QueryExclude x <- fst raw]
  within   <- do
    getWithins <- pure $ [x | QueryWithin x <- fst raw]
    getWithin <- case getWithins of
      []      -> pure $ Nothing
      (win:_) -> case parse parseQuery "" win of
        Left  _ -> pure $ Nothing
        Right w -> pure $ Just w
    return getWithin
  fuzzy <- pure $ fuzzyCmd ++ snd raw
  return $ Query fuzzy exact within

parseQueryValueList :: Parser ([QueryValue], [T.Text])
parseQueryValueList = do
  value <- many parseValue
  leftover <- T.toLower <$> T.pack <$> many printChar
  result <- pure $ ( catMaybes $ map snd value
                   , filter (not . T.null) $ leftover : map fst value )
  return result

parseValue :: Parser (T.Text, Maybe QueryValue)
parseValue = do
  prefix <- try $ rawContent
  cmd <- case snd prefix of
           --"#fpath{"   -> Just <$> parseFPath
           "#fuzzy{"   -> Just <$> parseFuzzy
           "#exact{"   -> Just <$> parseExact
           --"#exclude{" -> Just <$> parseExclude
           "#within{"  -> Just <$> parseWithin
           _           -> pure Nothing
  return (T.toLower $ T.pack $ fst prefix, cmd)

--parseFPath :: Parser QueryValue
--parseFPath = do
--  content <- QueryFPath <$> map T.unpack <$> exactContent
--  return content

parseFuzzy :: Parser QueryValue
parseFuzzy = do
  content <- QueryFuzzy <$> cmdContent
  return content

parseExact :: Parser QueryValue
parseExact = do
  content <- QueryExact <$> exactContent
  return content

--parseExclude :: Parser QueryValue
--parseExclude = do
--  content <- QueryExclude <$> cmdContent
--  return content

parseWithin :: Parser QueryValue
parseWithin = do
  content <- QueryWithin <$> queryContent
  return content

queryContent :: Parser T.Text
queryContent = T.pack <$> manyTill printChar (try $ char '}' <* eof)

cmdContent :: Parser [T.Text]
cmdContent = map T.toLower <$> exactContent

exactContent :: Parser [T.Text]
exactContent = T.words <$> T.pack
                       <$> manyTill printChar (char '}')

rawContent :: Parser (String, T.Text)
rawContent = manyTill_ printChar (try $ choice [ string "#fuzzy{"
                                               , string "#exact{"
                                               --, string "#exclude{"
                                               , string "#within{"
                                               , string " "
                                               , string "\n"
                                               ])
