-- | Fetch.hs

{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Fetch where

import qualified Data.Text           as T
import qualified Data.Map.Strict     as M
import qualified Data.IntMap.Strict  as IM
import           Network.HTTP.Client (newManager, defaultManagerSettings)
import           Data.Proxy
import           Servant.API
import           Servant.Client

type DocumentMap = M.Map FilePath PageText
type PageText = IM.IntMap T.Text

type AllTermFrequency = M.Map FilePath Int

type InvertedIndex = M.Map Term Occurences
type Term = T.Text
type Occurences = M.Map FilePath PageOccurences
type PageOccurences = IM.IntMap Int

type InterpreterAPI = "documentmap"          :> Get '[JSON] DocumentMap
                 :<|> "alltermfrequency"     :> Get '[JSON] AllTermFrequency
                 :<|> "invertedindex"        :> Get '[JSON] InvertedIndex
                 :<|> "lcinvertedindex"      :> Get '[JSON] InvertedIndex
                 :<|> "suffixinvertedindex"  :> Get '[JSON] InvertedIndex

interpreterAPI :: Proxy InterpreterAPI
interpreterAPI = Proxy

daemonDocumentMap :: ClientM DocumentMap
daemonAllTermFrequency :: ClientM AllTermFrequency
daemonInvertedIndex :: ClientM InvertedIndex
daemonLcInvertedIndex :: ClientM InvertedIndex
daemonSuffixInvertedIndex :: ClientM InvertedIndex
daemonDocumentMap
  :<|> daemonAllTermFrequency
  :<|> daemonInvertedIndex
  :<|> daemonLcInvertedIndex
  :<|> daemonSuffixInvertedIndex = client interpreterAPI

--type AllDaemonData = (DocumentMap
--                     , AllTermFrequency
--                     , InvertedIndex
--                     , InvertedIndex
--                     , InvertedIndex)
data AllDaemonData = AllDaemonData
  { documentMap :: DocumentMap
  , allTermFrequency :: AllTermFrequency
  , invertedIndex :: InvertedIndex
  , lcInvertedIndex :: InvertedIndex
  , suffixInvertedIndex :: InvertedIndex
  } deriving (Eq, Show)


clients :: ClientM AllDaemonData
clients = AllDaemonData <$> daemonDocumentMap
                        <*> daemonAllTermFrequency
                        <*> daemonInvertedIndex
                        <*> daemonLcInvertedIndex
                        <*> daemonSuffixInvertedIndex

fetch :: String -> IO (Either ClientError AllDaemonData)
fetch url = do
  manager' <- newManager defaultManagerSettings
  baseUrl <- parseBaseUrl url
  res <- runClientM clients (mkClientEnv manager' baseUrl)
  case res of
    Left err -> return $ Left err
    Right datas -> return $ Right datas

--fetchData ::
--  (AllDaemonData -> b)
--  -> Either ClientError AllDaemonData
--  -> Either ClientError b
--fetchData _ (Left  x) = Left x
--fetchData f (Right x) = Right $ f x
--fetchData :: a -> Either ClientError AllDaemonData -> Either ClientError a
--fetchData _ (Left  x) = Left x
--fetchData f (Right x) = Right $ x { f }
--fetchData ::
--  (AllDaemonData -> b)
--  -> IO (Either ClientError AllDaemonData)
--  -> IO (Either ClientError b)
--fetchData dat fet = do
--  fd <- fet
--  return $ dat <$> fd

--fetchDocumentMap :: IO (Either ClientError DocumentMap)
--fetchDocumentMap = fetchData documentMap fetch
--
--fetchAllTermFrequency :: IO (Either ClientError AllTermFrequency)
--fetchAllTermFrequency = fetchData allTermFrequency fetch
--
--fetchInvertedIndex :: IO (Either ClientError InvertedIndex)
--fetchInvertedIndex = fetchData invertedIndex fetch
--
--fetchLcInvertedIndex :: IO (Either ClientError InvertedIndex)
--fetchLcInvertedIndex = fetchData lcInvertedIndex fetch
--
--fetchSuffixInvertedIndex :: IO (Either ClientError InvertedIndex)
--fetchSuffixInvertedIndex = fetchData suffixInvertedIndex fetch

--fst' (x,_,_,_,_) = x
--snd' (_,x,_,_,_) = x
--thd  (_,_,x,_,_) = x
--fth  (_,_,_,x,_) = x
--sxth (_,_,_,_,x) = x
