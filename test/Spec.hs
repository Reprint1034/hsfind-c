{-# LANGUAGE OverloadedStrings #-}

import           Test.Hspec
--import           Test.Hspec.QuickCheck
import           Test.Hspec.Megaparsec
--import qualified Test.QuickCheck                      as Q
--import           Test.QuickCheck.Instances.Text
--import           Test.QuickCheck.Instances.Containers
--import           Test.QuickCheck.Instances
import           Text.Megaparsec
import           Query
import           Fetch
import           Evaluation
import qualified Data.Text                            as T
import qualified Data.IntMap.Strict                   as IM
import qualified Data.ByteString                      as B
import           Data.Aeson
import           Debug.Trace

--decodeFile f = decode <$> readFile f :: May
testDocumentMap = decodeFileStrict "./test/documentmap.json"
testAllTermFrequency = decodeFileStrict "./test/alltermfrequency.json"
testInvertedIndex = decodeFileStrict "./test/invertedindex.json"
testLcInvertedIndex = decodeFileStrict "./test/lcinvertedindex.json"
testSuffixInvertedIndex = decodeFileStrict "./test/suffixinvertedindex.json"
testQuery = T.pack "turing"

main :: IO ()
main = hspec $ do
  describe "Parsing user input into Query" $ do
    it "can parse a single term" $ do
      parse parseQuery "" (T.pack "word")
        `shouldParse` Query { fuzzy  = ["word"]
                            , exact  = []
                            , within = Nothing
                            }

    it "can parse multiple terms" $ do
      parse parseQuery "" (T.pack "a bunch of words")
        `shouldParse` Query { fuzzy  = ["words", "a", "bunch", "of"]
                            , exact  = []
                            , within = Nothing
                            }

    it "can parse terms under #fuzzy and #exact commands" $ do
      parse parseQuery ""
        (T.pack "some #fuzzy{Fuzzy} and #exact{Exact words} together")
        `shouldParse` Query { fuzzy  = [ "fuzzy"
                                       , "together"
                                       , "some"
                                       , "and"
                                       ]
                            , exact  = ["Exact", "words"]
                            , within = Nothing
                            }

    it "can parse terms recursively under #within commands" $ do
      parse parseQuery "" (T.pack "a word #within{another}")
        `shouldParse` Query { fuzzy  = ["a" , "word"]
                            , exact  = []
                            , within = Just Query { fuzzy  = ["another"]
                                                  , exact  = []
                                                  , within = Nothing
                                                  }
                            }

    it "can not parse any after the #within command is called" $ do
      parse parseQuery ""
        `shouldFailOn` (T.pack "a word #within{another} leftover")

  describe "Evaluating Query into SearchResult" $ do
    it "exists word from Query in all SearchResult elements" $ do
      dMap <- testDocumentMap
      aTF <- testAllTermFrequency
      invIndex <- testInvertedIndex
      lcInvIndex <- testLcInvertedIndex
      suffixInvIndex <- testSuffixInvertedIndex
      allDaemonData <- pure $ AllDaemonData <$> dMap
                                            <*> aTF
                                            <*> invIndex
                                            <*> lcInvIndex
                                            <*> suffixInvIndex
      allDaemonData `shouldSatisfy`
        \add -> case parse parseQuery "" testQuery of
            Left  _ -> False
            Right q -> case add of
              Nothing -> False
              Just a  -> existsQueryInSearchResult q $ evaluateQuery q a

--instance Q.Arbitrary Query where
--  arbitrary = Query <$> Q.arbitrary <*> Q.arbitrary <*> Q.arbitrary
--
--instance Q.Arbitrary AllDaemonData where
--  arbitrary = AllDaemonData <$> Q.arbitrary
--                            <*> Q.arbitrary
--                            <*> Q.arbitrary
--                            <*> Q.arbitrary
--                            <*> Q.arbitrary
--
--arbitraryPrintable = Q.getPrintableString <$> Q.arbitrary

existsQueryInSearchResult :: Query -> SearchResult -> Bool
existsQueryInSearchResult q = existsTermsInSearchResult (allQueryTerms q)

existsTermsInSearchResult :: [Term] -> SearchResult -> Bool
existsTermsInSearchResult terms =
  all (==True) . map (\(_,_,pt) -> existsTermsInPageText terms pt)
  where
    existsTermsInPageText ts =
      all (==True) . IM.map (\x ->
                               all (\t -> T.isInfixOf t $ T.toLower x) ts)

allQueryTerms :: Query -> [Term]
allQueryTerms q@Query{within = Nothing} =
  fuzzy q ++ (map T.toLower $ exact q)
allQueryTerms q@Query{within = Just  i} =
  fuzzy q ++ (map T.toLower $ exact q) ++ allQueryTerms i
