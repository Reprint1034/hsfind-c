{-# LANGUAGE OverloadedStrings #-}

module Main (main) where

import           Fetch
import           Evaluation
import           Query
import           Text.Pretty.Simple (pPrint)
import           Text.Megaparsec
import           System.Environment
import qualified Data.Text                   as T
import           Data.List

runQueryParser args = parse parseQuery "" args

printEvaluateQuery daemondata args = do
  query <- pure $ runQueryParser $ T.pack args
  case daemondata of
    Left x -> pPrint x
    Right x -> case query of
      Left y -> pPrint y
      Right y -> pPrint $ evaluateQuery y x

main :: IO ()
main = do
  args <- getArgs
  case args of
    []     -> putStrLn "No URL input"
    (x:xs) -> do
      fetchDaemon <- fetch x
      concatArgs <- pure $ intercalate " " xs
      printEvaluateQuery fetchDaemon concatArgs
